#! /bin/bash -x

. ./env

mkdir -p assets/blobs/plugins
echo "Fetching GeoServer Version (${GEOSERVER_VERSION})"
curl -s -L -o assets/blobs/geoserver-${GEOSERVER_VERSION}-war.zip -z assets/blobs/geoserver-${GEOSERVER_VERSION}-war.zip https://build.geoserver.org/geoserver/${GEOSERVER_MAJOR_VERSION}.x/geoserver-${GEOSERVER_MAJOR_VERSION}.x-latest-war.zip
curl -s -L -o assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-wps-plugin.zip -z assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-wps-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_MAJOR_VERSION}.x/ext-latest/geoserver-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT-wps-plugin.zip 
curl -s -L -o assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-vectortiles-plugin.zip -z ssets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-vectortiles-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_MAJOR_VERSION}.x/ext-latest/geoserver-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT-vectortiles-plugin.zip
curl -s -L -o assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-gdal-plugin.zip -z assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-gdal-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_MAJOR_VERSION}.x/ext-latest/geoserver-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT-gdal-plugin.zip
curl -s -L -o assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-hz-cluster-plugin.zip -z assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-hz-cluster-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_MAJOR_VERSION}.x/community-latest/geoserver-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT-hz-cluster-plugin.zip
curl -s -L -o assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-mbstyle-plugin.zip -z assets/blobs/plugins/geoserver-${GEOSERVER_VERSION}-mbstyle-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_MAJOR_VERSION}.x/community-latest/geoserver-${GEOSERVER_MAJOR_VERSION}-SNAPSHOT-mbstyle-plugin.zip

echo "Fetching Tomcat Version (${TOMCAT_VERSION})"
curl -s -L -o assets/blobs/apache-tomcat-${TOMCAT_VERSION}.tar.gz -z assets/blobs/apache-tomcat-${TOMCAT_VERSION}.tar.gz  https://archive.apache.org/dist/tomcat/tomcat-9/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz

echo "Fetching Marlin Renderer (${MARLIN_VERSION})"
curl -s -L -o assets/blobs/plugins/marlin-${MARLIN_VERSION}-Unsafe-sun-java2d.jar -z assets/blobs/plugins/marlin-${MARLIN_VERSION}-Unsafe-sun-java2d.jar https://github.com/bourgesl/marlin-renderer/releases/download/v${MARLIN_VERSION//\./_}/marlin-${MARLIN_VERSION}-Unsafe-sun-java2d.jar
curl -s -L -o assets/blobs/plugins/marlin-${MARLIN_VERSION}-Unsafe.jar -z assets/blobs/plugins/marlin-${MARLIN_VERSION}-Unsafe.jar https://github.com/bourgesl/marlin-renderer/releases/download/v${MARLIN_VERSION//\./_}/marlin-${MARLIN_VERSION}-Unsafe.jar

echo "Building Docker Image"

BUILD_ARGS=""
while read e; do
    BUILD_ARGS="${BUILD_ARGS} --build-arg $e";
done <env

echo $BUILD_ARGS
docker build --rm -t local/geoserver:${GEOSERVER_VERSION} ${BUILD_ARGS} .
docker tag local/geoserver:${GEOSERVER_VERSION} local/geoserver:latest