# FROM local/gdal:latest as geoserver
FROM centos:7
ARG GDAL_VERSION 
ARG GEOSERVER_VERSION 
ARG TOMCAT_VERSION
ARG MARLIN_VERSION

RUN set -x && \
	yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel unzip dejavu-fonts postgresql96 libcurl libsqlite3x sqlite3x wget openssl libxml2 proj49 && \
	yum -y clean all

ENV BUILD=/workspace TOMCAT_HOME=/tomcat PATH="/opt/gdal/bin:${PATH}"  GEOSERVER_DATA_DIR=/geoserver/config GEOSERVER_DATA=/geoserver/data

COPY assets/blobs/apache-tomcat-${TOMCAT_VERSION}.tar.gz ${BUILD}/
COPY assets/blobs/geoserver-${GEOSERVER_VERSION}-war.zip ${BUILD}/
COPY assets/blobs/plugins ${BUILD}/plugins
COPY assets/install_geoserver.sh ${BUILD}/
COPY assets/entrypoint.sh /entrypoint.sh
COPY assets/config ${GEOSERVER_DATA_DIR}

RUN ${BUILD}/install_geoserver.sh

COPY assets/index.html ${TOMCAT_HOME}/webapps/ROOT/index.html

VOLUME ${GEOSERVER_DATA_DIR}
VOLUME ${GEOSERVER_DATA}

EXPOSE 8080

CMD [ "su", "-m", "-c", "/entrypoint.sh", "tomcat"]



