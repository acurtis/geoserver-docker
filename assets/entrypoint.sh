#! /usr/bin/env bash


MARLIN_JAR=$(ls /tomcat/webapps/geoserver/WEB-INF/lib/marlin-*-Unsafe.jar)
MARLIN_2D_JAR=$(ls /tomcat/webapps/geoserver/WEB-INF/lib/marlin-*-java2d.jar)
MARLIN=""

if [[ -n "${MARLIN_JAR}" ]]; then
	MARLIN="-Xbootclasspath/a:${MARLIN_JAR} -Dsun.java2d.renderer=org.marlin.pisces.PiscesRenderingEngine"
fi
if [[ -n "${MARLIN_2D_JAR}" ]]; then
	MARLIN="${MARLIN} -Xbootclasspath/p:${MARLIN_2D_JAR}"
fi
export CATALINA_OPTS="${MARLIN} -Dorg.geotools.coverage.jaiext.enabled=true -Djava.library.path=/opt/gdal/lib"

${TOMCAT_HOME}/bin/catalina.sh run
